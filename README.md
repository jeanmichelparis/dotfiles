# Home directory
### Linux, Msys2, Cygwin
Configuration ***files*** and ***folder*** for my personal home directory.
Supported applications are:
`bash`
`zsh`
`mintty`
`emacs`
`ssh`

### Notes
1. Make sure to have <LF> as end line for all .bash files, else **Cygwin** can not deal with '\r' character.
2. **Notepad++** is very handy to do that